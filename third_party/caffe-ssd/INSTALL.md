# Installation

See http://caffe.berkeleyvision.org/installation.html for the latest
installation instructions.

Check the users group in case you need help:
https://groups.google.com/forum/#!forum/caffe-users

# Installation on ubuntu16.04

## Install needed packages

### Basic supports
> sudo apt-get install libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler
> sudo apt-get install --no-install-recommends libboost-all-dev

### ATLAS
> sudo apt-get install libatlas-base-dev
> sudo apt-get install libopenblas-dev

### Needed by ubuntu16.04 as well, though in the document it says only needed for 14.04
sudo apt-get install libgflags-dev libgoogle-glog-dev liblmdb-dev

## Build
> mkdir build
> cd build
> cmake ..
> make all -j4

## Install
> make install

## Install python dependencies
pip install scikit-image

