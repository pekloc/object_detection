== INSTALL CAFFE ==
follow CAFFE_INSTALL.md in the same directory

== INSTALL gstreamer ==
sudo apt install gstreamer-1.0

== INSTALL OPENCV ==
The opencv comes together with Jetpack doesn't support gstreamer. You need
to rebuild opencv by yourself.

You can follow the instruction below:

* git clone https://github.com/jetsonhacks/buildOpenCVTX2.git
* git checkout 16da404
* cd buildOpenCVTX2
* ./buildOpenCV.sh
* cd $HOME/opencv/build
* sudo make install
