#! /usr/bin/env python
from __future__ import print_function
import math
import os
import shutil
import stat
import subprocess
import logging
import sys

import path

sys.path.insert(0, path.CAFFE_PY_PATH)
import caffe
from caffe.model_libs import ConvBNLayer, VGGNetBody, CreateMultiBoxHead
from caffe.model_libs import caffe_pb2, P, L

PROJ_NAME = 'video'
NUM_CLASSES = 21
RESIZE = '300x300'
CODE_TYPE = P.PriorBox.CENTER_SIZE
MODEL_NAME = path.get_model_name()
TEST_BATCH_SIZE = 1
BACKGROUND_LABEL_ID=0
LR_MULT = 1.
SHARE_LOCATION = True

# Scale the image size for display.
SCALE = 0.8

SETTINGS = {
    'test_batch_size': 1,        # Number of frames to be processed per batch.
    'resize_height': 300,
    'resize_width': 300,
    'code_type': P.PriorBox.CENTER_SIZE,
    'use_batchnorm': False,
}


def add_extra_layers(net, use_batchnorm=True, lr_mult=1):
    """Add extra layers on top of a "base" network (e.g. VGGNet or Inception)."""
    use_relu = True

    # Add additional convolution layers.
    # 19 x 19
    from_layer = net.keys()[-1]

    # TODO(weiliu89): Construct the name using the last layer to avoid duplication.
    # 10 x 10
    out_layer = "conv6_1"
    ConvBNLayer(net, from_layer, out_layer, use_batchnorm, use_relu, 256, 1, 0, 1,
                lr_mult=lr_mult)

    from_layer = out_layer
    out_layer = "conv6_2"
    ConvBNLayer(net, from_layer, out_layer, use_batchnorm, use_relu, 512, 3, 1, 2,
                lr_mult=lr_mult)

    # 5 x 5
    from_layer = out_layer
    out_layer = "conv7_1"
    ConvBNLayer(net, from_layer, out_layer, use_batchnorm, use_relu, 128, 1, 0, 1,
                lr_mult=lr_mult)

    from_layer = out_layer
    out_layer = "conv7_2"
    ConvBNLayer(net, from_layer, out_layer, use_batchnorm, use_relu, 256, 3, 1, 2,
                lr_mult=lr_mult)

    # 3 x 3
    from_layer = out_layer
    out_layer = "conv8_1"
    ConvBNLayer(net, from_layer, out_layer, use_batchnorm, use_relu, 128, 1, 0, 1,
                lr_mult=lr_mult)

    from_layer = out_layer
    out_layer = "conv8_2"
    ConvBNLayer(net, from_layer, out_layer, use_batchnorm, use_relu, 256, 3, 0, 1,
                lr_mult=lr_mult)

    # 1 x 1
    from_layer = out_layer
    out_layer = "conv9_1"
    ConvBNLayer(net, from_layer, out_layer, use_batchnorm, use_relu, 128, 1, 0, 1,
                lr_mult=lr_mult)

    from_layer = out_layer
    out_layer = "conv9_2"
    ConvBNLayer(net, from_layer, out_layer, use_batchnorm, use_relu, 256, 3, 0, 1,
                lr_mult=lr_mult)

    return net


def create_test_net(settings):
    # Create test net.
    net = caffe.NetSpec()

    net.data = _prepare_data(settings)

    VGGNetBody(net, from_layer='data', fully_conv=True, reduced=True, dilated=True,
               dropout=False)
    add_extra_layers(net, settings['use_batchnorm'], lr_mult=LR_MULT)
    mbox_layers = gen_mbox_layers(net, code_type=settings['code_type'])
    mbox_layers.append(net.data)

    net.detection_out = L.DetectionOutput(*mbox_layers,
                                          detection_output_param=get_gen_det_param(),
                                          transform_param=get_output_transform_param(),
                                          include=dict(phase=caffe_pb2.Phase.Value('TEST')))
    net.slience = L.Silence(net.detection_out,
                            ntop=0,
                            include=dict(phase=caffe_pb2.Phase.Value('TEST')))

    test_net_file = path.get_test_net_file(PROJ_NAME)
    with open(test_net_file, 'w') as tn_fh:
        print('name: "{}_test"'.format(MODEL_NAME), file=tn_fh)
        print(net.to_proto(), file=tn_fh)
    shutil.copy(test_net_file, path.get_job_dir(PROJ_NAME))
    return test_net_file


def _prepare_data(settings):
    video_data_param = {'video_type': P.VideoData.VIDEO,
                        'video_file': path.VIDEO_FILE}
    test_transform_param = {
        'mean_value': [104, 117, 123],
        'resize_param': {
                'prob': 1,
                'resize_mode': P.Resize.WARP,
                'height': settings['resize_height'],
                'width': settings['resize_width'],
                'interp_mode': [P.Resize.LINEAR],
                },
        }
    return L.VideoData(video_data_param=video_data_param,
                       data_param=dict(batch_size=settings['test_batch_size']),
                       transform_param=test_transform_param)


def gen_mbox_layers(net,
                    code_type=P.PriorBox.CENTER_SIZE,
                    use_batch_norm=False,
                    conf_loss_type=P.MultiBoxLoss.SOFTMAX):
    """"Generate multi-box layers."""
    # conv4_3 ==> 38 x 38
    # fc7 ==> 19 x 19
    # conv6_2 ==> 10 x 10
    # conv7_2 ==> 5 x 5
    # conv8_2 ==> 3 x 3
    # conv9_2 ==> 1 x 1
    mbox_source_layers = ['conv4_3', 'fc7', 'conv6_2', 'conv7_2', 'conv8_2', 'conv9_2']
    min_sizes, max_sizes = gen_sizes(mbox_source_layers)
    # variance used to encode/decode prior bboxes
    if code_type == P.PriorBox.CENTER_SIZE:
        prior_variance = [0.1, 0.1, 0.2, 0.2]
    else:
        prior_variance = [0.1]

    mbox_layers = CreateMultiBoxHead(net,
                                     data_layer='data',
                                     from_layers=mbox_source_layers,
                                     use_batchnorm=use_batch_norm,
                                     min_sizes=min_sizes,
                                     max_sizes=max_sizes,
                                     aspect_ratios=[[2], [2, 3], [2, 3], [2, 3], [2], [2]],
                                     steps=[8, 16, 32, 64, 100, 300],
                                     normalizations=[20, -1, -1, -1, -1, -1],  # L2 normalize conv4_3
                                     num_classes=NUM_CLASSES,
                                     share_location=SHARE_LOCATION,
                                     flip=True,
                                     clip=False,
                                     prior_variance=prior_variance,
                                     kernel_size=3,
                                     pad=1,
                                     lr_mult=LR_MULT)

    conf_name = "mbox_conf"
    if conf_loss_type == P.MultiBoxLoss.SOFTMAX:
        reshape_name = "{}_reshape".format(conf_name)
        net[reshape_name] = L.Reshape(net[conf_name], shape=dict(dim=[0, -1, NUM_CLASSES]))
        softmax_name = "{}_softmax".format(conf_name)  # type: str
        net[softmax_name] = L.Softmax(net[reshape_name], axis=2)
        flatten_name = "{}_flatten".format(conf_name)
        net[flatten_name] = L.Flatten(net[softmax_name], axis=1)
        mbox_layers[1] = net[flatten_name]
    elif conf_loss_type == P.MultiBoxLoss.LOGISTIC:
        sigmoid_name = "{}_sigmoid".format(conf_name)
        net[sigmoid_name] = L.Sigmoid(net[conf_name])
        mbox_layers[1] = net[sigmoid_name]

    return mbox_layers


def gen_sizes(mbox_source_layers):
    # minimum dimension of input image
    min_dim = 300
    # in percent %
    min_ratio = 20
    max_ratio = 90
    step = int(math.floor((max_ratio - min_ratio) / (len(mbox_source_layers) - 2)))
    min_sizes = []
    max_sizes = []
    for ratio in range(min_ratio, max_ratio + 1, step):
        min_sizes.append(min_dim * ratio / 100.)
        max_sizes.append(min_dim * (ratio + step) / 100.)
    min_sizes = [min_dim * 10 / 100.] + min_sizes
    max_sizes = [min_dim * 20 / 100.] + max_sizes
    return min_sizes, max_sizes


def get_gen_det_param():
    """Get parameters for generating detection output."""
    # Only display high quality detections whose scores are higher than a threshold.
    visualize_threshold = 0.3
    return {
        'num_classes': NUM_CLASSES,
        'share_location': SHARE_LOCATION,
        'background_label_id': BACKGROUND_LABEL_ID,
        'nms_param': {'nms_threshold': 0.45, 'top_k': 400},
        'save_output_param': {
                'label_map_file': path.LABEL_MAP_FILE,
                },
        'keep_top_k': 200,
        'confidence_threshold': 0.01,
        'code_type': CODE_TYPE,
        'visualize': True,
        'visualize_threshold': visualize_threshold,
        'save_file': os.path.join(path.get_save_dir(PROJ_NAME), 'result.avi'),
        }


def get_output_transform_param():
    # Size of video image.
    video_width = 1280
    video_height = 720
    return {
            'mean_value': [104, 117, 123],
            'resize_param': {
                    'prob': 1,
                    'resize_mode': P.Resize.WARP,
                    'height': int(video_height * SCALE),
                    'width': int(video_width * SCALE),
                    'interp_mode': [P.Resize.LINEAR],
                    },
            }


def create_job_file(test_net_file, solver_mode=P.Solver.GPU):
    """Create job file."""
    # Set the number of test iterations to the maximum integer number.
    test_iter = int(math.pow(2, 29) - 1)
    job_file = path.get_job_script(PROJ_NAME)
    with open(job_file, 'w') as f:
        f.write('{} test \\\n'.format(path.CAFFE_CMD))
        f.write('--model="{}" \\\n'.format(test_net_file))
        f.write('--weights="{}" \\\n'.format(_get_latest_snapshot()))
        f.write('--iterations="{}" \\\n'.format(test_iter))
        if solver_mode == P.Solver.GPU:
            f.write('--gpu 0\n')

    # Add executable permission.
    os.chmod(job_file, stat.S_IRWXU)

    # Copy the python script to job_dir.
    py_file = os.path.abspath(__file__)
    shutil.copy(py_file, path.get_job_dir(PROJ_NAME))
    return job_file


def _get_latest_snapshot():
    """Get the latest snapshot.

    Returns:
        str: full path to the latest snapshot file

    """
    snapshot_dir = path.get_pre_trained_dir()
    max_iter = 0
    for file_ in os.listdir(snapshot_dir):
        if file_.endswith(".caffemodel"):
            basename = os.path.splitext(file_)[0]
            iter_ = int(basename.split('{}_iter_'.format(MODEL_NAME))[1])
            if iter_ > max_iter:
                max_iter = iter_

    if max_iter == 0:
        msg = 'Cannot find snapshot in {}'.format(snapshot_dir)
        raise OSError(msg)
    snapshot_prefix = "{}/{}".format(snapshot_dir, path.get_model_name())
    return "{}_iter_{}.caffemodel".format(snapshot_prefix, max_iter)


def run_job(job_file):
    subprocess.call(job_file, shell=True)


def prepare_dirs():
    for path_ in [path.get_job_dir(PROJ_NAME), path.get_save_dir(PROJ_NAME)]:
        _create_dir(path_)


def _create_dir(path_):
    logging.debug('Preparing directory: %s', path_)
    if not os.path.exists(path_):
        os.makedirs(path_)


def main():
    """Main entry of the program."""
    logging.basicConfig(level=logging.DEBUG)
    prepare_dirs()
    test_net_file = create_test_net(SETTINGS)
    job_file = create_job_file(test_net_file)
    run_job(job_file)


if __name__ == '__main__':
    sys.exit(main())
