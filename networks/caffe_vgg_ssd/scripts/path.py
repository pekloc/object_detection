from __future__ import print_function
import os

REPO_ROOT = os.path.realpath(os.path.join(os.path.dirname(__file__),
                                          '..', '..', '..'))
CAFFE_ROOT = os.path.join(REPO_ROOT, 'third_party', 'caffe-ssd')
CAFFE_PY_PATH = os.path.join(CAFFE_ROOT, 'python')
CAFFE_CMD = os.path.join(CAFFE_ROOT, 'build', 'tools', 'caffe')

PROJ_ROOT = os.path.join(REPO_ROOT, 'networks', 'caffe_vgg_ssd')
MODEL_ROOT = os.path.join(PROJ_ROOT, 'models')
JOBS_ROOT = os.path.join(PROJ_ROOT, 'jobs')
DEMO_DIR = os.path.join(PROJ_ROOT, 'demo')
VIDEO_FILE = os.path.join(DEMO_DIR, 'ILSVRC2015_train_00755001.mp4')
LABEL_MAP_FILE = os.path.join(PROJ_ROOT, 'data', 'VOC0712', 'labelmap_voc.prototxt')

DEFULAT_RESIZE = '300x300'


def get_pre_trained_dir(resize=DEFULAT_RESIZE):
    """Return the directory where snapshot is stored.

    Args:
        resize (int): the resize name in string, e.g. '300X300'

    Returns:
        str: the model name
    """
    return '{}/downloaded/{}'.format(MODEL_ROOT, get_job_name(resize))


def get_model_name(resize=DEFULAT_RESIZE):
    """Get the model's name.

    Args:
        resize (int): the resize name in string, e.g. '300X300'

    Returns:
        str: the model name

    """
    return 'VGG_VOC0712_{}'.format(get_job_name(resize))


def get_job_name(resize=DEFULAT_RESIZE):
    """Return the training/testing jobs name.

    Args:
        resize (int): the resize name in string, e.g. '300X300'

    Returns:
        str: the job name

    """
    return 'SSD_{}'.format(resize)


def get_job_dir(name, resize=DEFULAT_RESIZE):
    """Get the directory which stores the job script and log file.

    Args:
        name (str): more specific job name
        resize (int): the resize name in string, e.g. '300X300'

    Returns:
        str: full path to the job directory

    """
    return '{}/{}_{}'.format(JOBS_ROOT, get_job_name(resize), name)


def get_save_dir(name, resize=DEFULAT_RESIZE):
    """Get the directory which stores the model .prototxt file.

    Args:
        name (str): more specific job name
        resize (int): the resize name in string, e.g. '300X300'

    Returns:
        str: full path to the save directory

    """
    return '{}/{}_{}'.format(MODEL_ROOT, get_job_name(resize), name)


def get_test_net_file(name, resize=DEFULAT_RESIZE):
    """Get model definition file."""
    return os.path.join(get_save_dir(name, resize), 'test.prototxt')


def get_job_script(name, resize=DEFULAT_RESIZE):
    """Get the job script.

    Args:
        name (str): more specific job name
        resize (int): the resize name in string, e.g. '300X300'

    Returns:
        str: full path to the job script

    """
    return os.path.join(get_job_dir(name, resize),
                       '{}.sh'.format(get_model_name(resize)))
