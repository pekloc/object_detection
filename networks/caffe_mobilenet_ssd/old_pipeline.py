#! /usr/bin/env python

import sys
import gi
import cv2 as cv
import numpy as np
import time
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst, GstApp
from PIL import Image
Gst.init(None)
image = None




class Player(object):
    def init(self):
        exit_code = 0
        count = 0
        self.pipeline = Gst.Pipeline()                                #Build a empty pipeline

        self.udp_src = Gst.ElementFactory.make('udpsrc', 'udp_src')   #Named udpsrc

        self.udp_src.set_property('port', 5000)                       #Set the property of udpsrc, port=5000
        self.pipeline.add(self.udp_src)                                    #Put udpsrc into pipeline

        self.depay = Gst.ElementFactory.make('rtph264depay', 'depay')
        self.pipeline.add(self.depay)

        self.uqueue = Gst.ElementFactory.make('queue', 'uqueue')
        self.pipeline.add(self.uqueue)

        self.dec = Gst.ElementFactory.make('avdec_h264', 'dec')
        if not self.dec:
            print "create dec failed"
        self.pipeline.add(self.dec)

        #video_sink = Gst.ElementFactory.make('xvimagesink', 'video_sink')
        #pipeline.add(video_sink)
        #video_sink.set_property('sync', 'false')
        #video_sink.set_property('async', 'false')
        self.convert = Gst.ElementFactory.make('videoconvert', 'convert')
        self.pipeline.add(self.convert)
        self.app_sink = Gst.ElementFactory.make('appsink', 'app_sink')
        self.app_sink.set_property('emit-signals', True)
        cap = Gst.caps_from_string('video/x-raw,width=672, height=376, format=BGR')
        self.app_sink.set_property('caps', cap)
        # self.app_sink.connect("new-sample", pull_sample, self.app_sink)

        self.pipeline.add(self.app_sink)

        self.udp_src.link_filtered(self.depay, Gst.caps_from_string('application/x-rtp, encoding-name=H264, payload=96'))
        #Set the sink pad of rtph264depay with Gst.caps_from_string() and use link_filtered to link udpsrc and rtph264depay
        self.depay.link(self.uqueue)
        #Link rtph264depay and queue
        self.uqueue.link(self.dec)
        self.dec.link(self.convert)
        self.convert.link(self.app_sink)
        #dec.link(video_sink)
        #dec.link_filtered(video_sink, Gst.caps_from_string('video/x-raw, width=672, height=376, format=I420'))
        #self.dec.link_filtered(self.app_sink, Gst.caps_from_string('video/x-raw,width=672, height=376, format=I420'))
        #The default intput of xvimagesink is I420, replace xvimagesink with appsink.
    def pipeline_pull_sample(self):
        global image
        sample = self.app_sink.pull_sample()
        time.sleep(0.030)
        image = gst_to_opencv(sample)
        # Gst.GObject.unref(sample)
        print image.shape
        return 0

    def start_pipeline(self):
        self.pipeline.set_state(Gst.State.PLAYING)  #start pipeline
        print "pipeline is PLAYING...."

    def stop_pipeline(self):
        self.pipeline.set_state(Gst.State.NULL)
        self.pipeline.unref
        print "pipeline is STOP..."


def gst_to_opencv(sample):
    buf = sample.get_buffer()
    # result, map = buf.map(Gst.MapFlags.READ)
    caps = sample.get_caps()
    print caps.get_structure(0).get_value('format')
    print caps.get_structure(0).get_value('height')
    print caps.get_structure(0).get_value('width')

    print buf.get_size()
    # print map.size
    # if result:
    #     data = map.data

    #     return data
    # else:
    #     print "get image error"
    #     return None
    arr = np.ndarray(
            shape=(caps.get_structure(0).get_value('height'),
            caps.get_structure(0).get_value('width'),3),
            buffer=buf.extract_dup(0, buf.get_size()),
            dtype=np.uint8)
    return arr

def pull_sample(sample,sink):
    global image
    # sample = sink.emit("pull-sample")
    image = gst_to_opencv(sample)
    print image.shape
    return 0


p = Player()
p.init()
p.start_pipeline()
while True:
    p.pipeline_pull_sample()
    if image is not None:
        cv.imshow("appsink data", image)
        image = None
        cv.waitKey(1)
p.stop_pipeline()
#if __name__ == '__main__':
#    sys.exit(init())

