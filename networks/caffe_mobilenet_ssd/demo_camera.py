#! /usr/bin/env python
import sys
import logging

import cv2

import _utils

sys.path.insert(0, _utils.get_caffe_py_path())
import caffe
import _inference

WINDOW_NAME = 'test-onboard-camera'


def read_cam_and_classify(cap, net, detect_func):
    show_full_screen = False
    while True:
        if cv2.getWindowProperty(WINDOW_NAME, 0) < 0:  # Check to see if the user closed the window
            break

        _ret_val, img = cap.read()
        img = detect_func(net, img)
        cv2.imshow(WINDOW_NAME, img)

        key = cv2.waitKey(10)
        if key == 27: # ESC key: quit program
            break
        elif key == ord('F') or key == ord('f'):  # toggle full screen
            show_full_screen = not show_full_screen
            if show_full_screen:
                cv2.setWindowProperty(WINDOW_NAME, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            else:
                cv2.setWindowProperty(WINDOW_NAME, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL)


def open_cam_onboard(width, height):
    # On versions of L4T previous to L4T 28.1, flip-method=2
    # Use Jetson onboard camera
    gst_str = ('nvcamerasrc '
               '! video/x-raw(memory:NVMM), '
               'width=(int)2592, height=(int)1458, '
               'format=(string)I420, framerate=(fraction)30/1 '
               '! nvvidconv '
               '! video/x-raw, '
               'width=(int){}, height=(int){}, format=(string)BGRx '
               '! videoconvert '
               '! appsink').format(width, height)
    logging.info('gst_str: %s', gst_str)
    cap = cv2.VideoCapture(gst_str, cv2.CAP_GSTREAMER)
    if not cap.isOpened():
        cap.release()
        raise RuntimeError('Failed to open camera!')
    return cap


def open_window(width, height):
    cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(WINDOW_NAME, width, height)
    cv2.moveWindow(WINDOW_NAME, 0, 0)
    cv2.setWindowTitle(WINDOW_NAME, 'DEMO')


def main():
    exit_code = 0
    logging.basicConfig(level=logging.DEBUG)
    image_height = 1458
    image_width = 2592
    logging.info('image height: %s, width: %s', image_height, image_width)

    caffe.set_mode_gpu()
    try:
        cap = open_cam_onboard(image_width, image_height)
        open_window(image_width, image_height)
        net = _inference.get_network()
        read_cam_and_classify(cap, net, _inference.detect)
    except Exception as error:
        logging.error('Something wrong: %s', str(error), exc_info=True)
        exit_code = 1
    finally:
        cv2.destroyAllWindows()
    return exit_code


if __name__ == '__main__':
    sys.exit(main())
