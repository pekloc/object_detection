"""This module provides the network inference implementation."""
import os
import time
import logging

import numpy as np
import cv2
import caffe

PWD = os.path.realpath(os.path.dirname(__file__))
MODEL_DIR = os.path.join(PWD, 'model')

NET_FILE= os.path.join(MODEL_DIR, 'MobileNetSSD_deploy.prototxt')
CAFFE_MODEL = os.path.join(MODEL_DIR, 'MobileNetSSD_deploy.caffemodel')

CLASSES = ('background',
           'aeroplane', 'bicycle', 'bird', 'boat',
           'bottle', 'bus', 'car', 'cat', 'chair',
           'cow', 'diningtable', 'dog', 'horse',
           'motorbike', 'person', 'pottedplant',
           'sheep', 'sofa', 'train', 'tvmonitor')


def get_network():
    """Get the pre-trained network.

    Returns:
        caffe.Net: pretrained network

    """
    if not os.path.exists(CAFFE_MODEL):
        msg = 'Pretrainded network {} does not exist!'.format(CAFFE_MODEL)
        raise RuntimeError(msg)
    if not os.path.exists(NET_FILE):
        msg = 'Network definition file {} does not exist!'.format(NET_FILE)
        raise RuntimeError(msg)
    net = caffe.Net(NET_FILE, CAFFE_MODEL, caffe.TEST)
    return net


def detect(net, origin_img):
    """Detect objects on the the input image.

    Args:
        net (caffe.Net): the object detection network
        origin_img (np.ndarray): image to detect object

    Returns:
        output_img (np.ndarray): image with detected objects plotted

    """
    start_time = time.time()
    img = _pre_process(origin_img)

    net.blobs['data'].data[...] = img
    out = net.forward()
    box, conf, cls = _post_process(origin_img.shape[0], origin_img.shape[1], out)

    output_img = _plot_objects(origin_img.copy(), box, cls)

    time_used = time.time() - start_time
    logging.info('Time used: %s, FPS: %s', time_used, 1 / time_used)
    return output_img


def _pre_process(input_img):
    """Pre-process the image to input to network."""
    img = cv2.resize(input_img, (300,300))
    img = (img - 127.5) * 0.007843
    img = img.astype(np.float32)
    img = img.transpose((2, 0, 1))
    return img


def _post_process(origin_img_height, origin_img_width, out):
    """Post process the network output.

    Args:
        origin_img_height (int): the height of the original image
        origin_img_width (int): the width of the original image
        out (dict): network output

    Returns:
        tuple: (np.ndarray, list(float), list(int))

    """
    detects = out['detection_out']
    box = detects[0, 0, :, 3:7] * np.array([origin_img_width, origin_img_height,
                                            origin_img_width, origin_img_height])
    cls = detects[0, 0, :, 1]
    conf = detects[0, 0, :, 2]
    return box.astype(np.int32), conf, cls


def _plot_objects(base_img, boxes, cls):
    for index, box in enumerate(boxes):
        top_left = (box[0], box[1])
        bottom_right = (box[2], box[3])

        # plot the bounding box on the detected object, with green color and thickness 2
        cv2.rectangle(base_img, top_left, bottom_right, (0, 255, 0), 2)

        # plot the class name as text on the top left corner of the detected box
        title = '%s' % (CLASSES[int(cls[index])])
        p3_text = (max(top_left[0], 15), max(top_left[1], 15) - 5)
        cv2.putText(base_img, title, p3_text, cv2.FONT_ITALIC, 0.6, (0, 255, 0), 1)
    return base_img
