#! /usr/bin/env python
import sys
import logging
import os
import time
from threading import Event
from threading import Thread
from Queue import LifoQueue
from Queue import Empty

import numpy as np
import cv2
import gi
gi.require_version('Gst', '1.0')
gi.require_version('GstApp', '1.0')
from gi.repository import GObject, Gst, GstApp

import _utils

sys.path.insert(0, _utils.get_caffe_py_path())
import caffe

WINDOW_NAME = 'test-zed-camera'

IMAGE_HEIGHT = 720   #1458
IMAGE_WIDTH = 1280   #2592
flag = 0
PWD = os.path.realpath(os.path.dirname(__file__))
MODEL_DIR = os.path.join(PWD, 'model')

NET_FILE= os.path.join(MODEL_DIR, 'MobileNetSSD_deploy.prototxt')
CAFFE_MODEL = os.path.join(MODEL_DIR, 'MobileNetSSD_deploy.caffemodel')

CONF_THRESHOLD = 0.5
CLASSES = ('background',
           'aeroplane', 'bicycle', 'bird', 'boat',
           'bottle', 'bus', 'car', 'cat', 'chair',
           'cow', 'diningtable', 'dog', 'horse',
           'motorbike', 'person', 'pottedplant',
           'sheep', 'sofa', 'train', 'tvmonitor')


def get_network():
    if not os.path.exists(CAFFE_MODEL):
        msg = 'Pre-trained network {} does not exist!'.format(CAFFE_MODEL)
        raise RuntimeError(msg)
    if not os.path.exists(NET_FILE):
        msg = 'Network definition file {} does not exist!'.format(NET_FILE)
        raise RuntimeError(msg)
    net = caffe.Net(NET_FILE, CAFFE_MODEL, caffe.TEST)
    return net


def _pre_process(input_img):
    img = cv2.resize(input_img, (300,300))
    img = (img - 127.5) * 0.007843
    img = img.astype(np.float32)
    img = img.transpose((2, 0, 1))
    return img


def _post_process(origin_img_height, origin_img_width, out):
    detects = out['detection_out']
    box = detects[0, 0, :, 3:7] * np.array([origin_img_width, origin_img_height,
    origin_img_width, origin_img_height])
    cls = detects[0, 0, :, 1]
    conf = detects[0, 0, :, 2]
    return box.astype(np.int32), conf, cls


def _plot_objects(base_img, boxes, cls, conf):
    for index, box in enumerate(boxes):
        obj_cls = CLASSES[int(cls[index])]
        obj_conf = conf[index]
        logging.debug('detect class: %s, confidence: %s', obj_cls, obj_conf)
        if obj_cls in ['person'] and obj_conf >= CONF_THRESHOLD:
            top_left = (box[0], box[1])
            bottom_right = (box[2], box[3])

            # plot the bounding box on the detected object
            # with green color and thickness 2
            cv2.rectangle(base_img, top_left, bottom_right, (0, 255, 0), 2)
    return base_img


class Player(Thread):

    def __init__(self, image_queue, need_image_event, stop_event, src_port=5000):
        """Constructor."""
        self._image_queue = image_queue
        self._need_image_event = need_image_event
        self._stop_event = stop_event
        self._src_port = src_port
        self._pipeline, self._appsink = self._create()
        super(Player, self).__init__()

    def _create(self):
        """Create a gstreamer pipeline.

        udpsrc -> rtph264depay -> queue -> avdec_h264 -> video_convert -> appsink

        """
        pipeline = Gst.Pipeline()                                # Build a empty pipeline
        udp_src = Gst.ElementFactory.make('udpsrc', 'udp_src')   # Named udpsrc
        depay = Gst.ElementFactory.make('rtph264depay', 'depay')
        dec = Gst.ElementFactory.make('avdec_h264', 'dec')
        convert = Gst.ElementFactory.make('videoconvert', 'convert')
        app_sink = Gst.ElementFactory.make('appsink', 'app_sink')

        # video_convert = Gst.ElementFactory.make('videoconvert', 'video_convert')
        # video_sink = Gst.ElementFactory.make('xvimagesink', 'video_sink')
        # queue1 = Gst.ElementFactory.make('queue', 'queue1')
        # queue2 = Gst.ElementFactory.make('queue', 'queue2')
        # tee = Gst.ElementFactory.make('tee', 'tee')


        udp_src.set_property('port', self._src_port)             # Set the property of udpsrc, port=5000
        cap = Gst.caps_from_string('video/x-raw,width={}, height={}, format=BGR'.format(IMAGE_WIDTH, IMAGE_HEIGHT))
        app_sink.set_property('caps', cap)
        app_sink.set_property('drop', True)

        pipeline.add(udp_src)                                    # Put udpsrc into pipeline
        pipeline.add(depay)
        pipeline.add(dec)
        pipeline.add(convert)
        pipeline.add(app_sink)

        # pipeline.add(tee)
        # pipeline.add(queue1)
        # pipeline.add(queue2)
        # pipeline.add(video_convert)
        # pipeline.add(video_sink)

        # tee_pad_1 = tee.get_request_pad('src_1')
        # tee_pad_2 = tee.get_request_pad('src_2')
        # queue1_pad = queue1.get_static_pad('sink')
        # queue2_pad = queue2.get_static_pad('sink')


        # Set the sink pad of rtph264depay with Gst.caps_from_string()
        # and use link_filtered to link udpsrc and rtph264depay
        udp_src.link_filtered(depay,
                              Gst.caps_from_string('application/x-rtp, encoding-name=H264, payload=96'))
        logging.info('GST: pipeline is created!')
        depay.link(dec)
        # dec.link(tee)
        dec.link(convert)
        # tee_pad_1.link(queue1_pad)
        # queue1.link(convert)
        convert.link(app_sink)

        # tee_pad_2.link(queue2_pad)
        # queue2.link(video_convert)
        # video_convert.link(video_sink)
        return pipeline, app_sink

    def run(self):
        """Start to run the thread."""
        try:
            self.start_pipeline()
            while not self._stop_event.is_set():
                self.pull_sample()
            self.stop_pipeline()
        except Exception, error:
            logging.error('Something wrong in player thread, error: %s',
                          str(error), exc_info=True)

    def pull_sample(self):
        global flag
        sample = self._appsink.pull_sample()
        if self._need_image_event.is_set():
            zed_image = _convert_gst_to_numpy(sample)
            if flag is 1:
                flag = 0
                self._image_queue.put(zed_image)
                logging.debug('GST: got image with shape: %s', zed_image.shape)
            else:
                flag += 1

            # self._image_queue.put(zed_image)
            # logging.debug('GST: got image with shape: %s', zed_image.shape)
            # self._need_image_event.clear()

    def start_pipeline(self):
        self._pipeline.set_state(Gst.State.PLAYING)
        logging.info('GST: pipeline is PLAYING ...')

    def stop_pipeline(self):
        self._pipeline.set_state(Gst.State.NULL)
        logging.info('GST: pipeline is STOP ...')


def _convert_gst_to_numpy(sample):
    buf = sample.get_buffer()
    caps = sample.get_caps()
    cap_struct = caps.get_structure(0)
    height = cap_struct.get_value('height')
    width = cap_struct.get_value('width')
    logging.debug('Caps format: %s, height: %s, width: %s',
                  cap_struct.get_value('format'), height, width)

    arr = np.ndarray(shape=(height, width, 3),
                     buffer=buf.extract_dup(0, buf.get_size()),
                     dtype=np.uint8)

    return arr


def detect(net, origin_img):
    start_time = time.time()
    img = _pre_process(origin_img)

    net.blobs['data'].data[...] = img
    out = net.forward()
    box, conf, cls = _post_process(origin_img.shape[0], origin_img.shape[1], out)
    output_img = _plot_objects(origin_img.copy(), box, cls, conf)
    time_used = time.time() - start_time
    logging.info('OD: time used for a frame: %s, FPS: %s',
                 time_used, 1 / time_used)
    return output_img


def perform_object_detection(net, image_queue, need_image_event, stop_event,
                             show_full_screen=False):
    logging.debug('Start object detection')
    need_image_event.set()
    time_used = 0
    while not stop_event.is_set():
        try:
            zed_image = image_queue.get(timeout=1)
        except Empty:
            logging.warning('OD: waiting for incoming image ...')
            continue
        start_time = time.time()
        input_image = zed_image.copy()
        img = detect(net, input_image)
        if time_used is not 0:
            cv2.putText(img,'FPS: {}'.format((1/time_used)),(0,710), cv2.FONT_ITALIC, 0.6, (0,0,0), 2)
        cv2.imshow(WINDOW_NAME, img)

        key = cv2.waitKey(1)
        if key is ord('2'):  # toggle full screen
            show_full_screen = not show_full_screen
            if show_full_screen:
                cv2.setWindowProperty(WINDOW_NAME, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
            else:
                cv2.setWindowProperty(WINDOW_NAME, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_NORMAL)
        need_image_event.set()
        time_used = time.time() - start_time


def open_window(width, height):
    cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(WINDOW_NAME, width, height)
    cv2.moveWindow(WINDOW_NAME, 0, 0)
    cv2.setWindowTitle(WINDOW_NAME, 'demo_zed')


def main():
    exit_code = 0
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s %(message)s')
    caffe.set_mode_gpu()

    image_queue = LifoQueue()
    need_image_event = Event()
    stop_event = Event()
    try:
        Gst.init(None)
        player = Player(image_queue, need_image_event, stop_event)
        player.daemon = True
        player.start()

        net = get_network()
        open_window(960, 540)
        perform_object_detection(net, image_queue, need_image_event, stop_event)
    except KeyboardInterrupt:
        logging.warning('Keyboard interrupt detected! Going to exit program.')
        stop_event.set()
        exit_code = 1
    except Exception as error:
        logging.error('Something wrong: %s', str(error), exc_info=True)
        exit_code = 1
    finally:
        cv2.destroyAllWindows()
    return exit_code


if __name__ == '__main__':
    sys.exit(main())
