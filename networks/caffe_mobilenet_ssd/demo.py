#! /usr/bin/env python
import sys
import os
import logging

import cv2

import _utils

sys.path.insert(0, _utils.get_caffe_py_path())
import caffe
import _inference

PWD = os.path.realpath(os.path.dirname(__file__))
TEST_DIR = os.path.join(PWD, "images")
OUTPUT_DIR = os.path.join(PWD, 'outputs')


def main():
    exit_code = 0
    logging.basicConfig(level=logging.INFO)
    caffe.set_mode_gpu()
    net = _inference.get_network()

    for file_name in os.listdir(TEST_DIR):
        if file_name.startswith('.'):
            logging.info('skip file: {}'.format(file_name))
            continue
        input_img = cv2.imread(os.path.join(TEST_DIR, file_name))
        output_img = _inference.detect(net, input_img)
        cv2.imwrite(os.path.join(OUTPUT_DIR, file_name), output_img)

    return exit_code


if __name__ == '__main__':
    sys.exit(main())
