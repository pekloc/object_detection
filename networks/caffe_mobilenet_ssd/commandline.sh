#! /bin/bash
gst-launch-1.0 nvcamerasrc fpsRange="20.0 20.0" intent=3 ! nvvidconv flip-method=4  ! 'video/x-raw(memory:NVMM), width=(int)1280, height=(int)720, format=(string)I420, framerate=(fraction)30/1' !  omxh264enc control-rate=2 bitrate=4000000 ! 'video/x-h264, stream-format=(string)byte-stream' !  h264parse ! rtph264pay mtu=1400 ! udpsink host=127.0.0.1 port=5000 sync=false async=false
