"""Utility functions."""
import os

PWD = os.path.realpath(os.path.dirname(__file__))


def get_caffe_path():
    """Get caffe installation's root path.

    Returns:
        str

    """
    return os.path.realpath(os.path.join(PWD, '..', '..', 'third_party', 'caffe-ssd'))


def get_caffe_py_path():
    """Get pycaffe's package root path.

    Returns:
        str

    """
    return os.path.join(get_caffe_path(), 'python')
